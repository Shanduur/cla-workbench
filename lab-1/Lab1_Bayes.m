clear all; close all; clc;

load('dane2.mat');
rng('default')

mu1 = [0; 0];
mu2 = [2; 2];

sigma1 = [2 -1; -1 2];
sigma2 = [1 0; 0 1];

A1 = mvnrnd(mu1,sigma1,100);
A2 = mvnrnd(mu2,sigma2,100);

A1 = [zeros(100, 1), A1];
A2 = [ones(100, 1), A2];
data = [A1; A2];

i = randperm(size(data,1));
trainingSet = data (i(1: 100), :);
testSet = data (i(101: end), :);

scatter(trainingSet(:,2),trainingSet(:,3),50,trainingSet(:,1), 'filled');

B1 = betarnd(1,1,[100, 10]);
B2 = betarnd(2,2,[100, 10]);

B1 = [zeros(100, 1), B1];
B2 = [ones(100, 1), B2];
dataB = [B1; B2];

i = randperm(size(dataB,1));
betaTrainingSet = dataB (i(1: 100), :);
betaTestSet = dataB (i(101: end), :);

load('dane2.mat');

realTrainingSet =[uczacy.D', uczacy.X'];
realTestSet =[testowy.D', testowy.X'];

[ParametricAccuracy] = BayesParametric(trainingSet, testSet);

[NonParametricAccuracy] = BayesNonParametric(trainingSet, testSet);

[ParametricAccuracyBeta] = BayesParametric(betaTrainingSet, betaTestSet);

[NonParametricAccuracyBeta] = BayesNonParametric(betaTrainingSet, betaTestSet);

[ParametricAccuracyReal] = BayesParametric(realTrainingSet, realTestSet);

[NonParametricAccuracyReal] = BayesNonParametric(realTrainingSet, realTestSet);

function [ParametricAccuracy] = BayesParametric(trainingSet, testSet)
[n,m]=size(testSet);

[m1, s1]=normfit(trainingSet(trainingSet(:,1)==0,2:m));
[m2, s2]=normfit(trainingSet(trainingSet(:,1)==1,2:m));

x = [];

for i=1:n
    p1=prod(normpdf(testSet(i,2:m),m1,s1));
    p2=prod(normpdf(testSet(i,2:m),m2,s2));
    if p1>p2
        x(i)=0;
    else
        x(i)=1;
    end
end

ParametricAccuracy=sum(x==testSet(:,1)')/n;
end

function [NonParametricAccuracy] = BayesNonParametric(trainingSet, testSet)
windowSize=2;
[n,m]=size(testSet);
for i=2:m
    [f1,x1] = ksdensity(trainingSet(trainingSet(:,1)==0,i),testSet(:,i),'Bandwidth',windowSize);
    [f2,x2] = ksdensity(trainingSet(trainingSet(:,1)==1,i),testSet(:,i),'Bandwidth',windowSize);
    F1(:,i-1) = f1;
    F2(:,i-1) = f2;
end

p1 = prod(F1,2);
p2 = prod(F2,2);
x = [];

for i=1:n
    if p1(i)>p2(i)
        x(i)=0;
    else
        x(i)=1;
    end
end

NonParametricAccuracy=sum(x==testSet(:,1)')/n;
end
